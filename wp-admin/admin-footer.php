<?php
/**
 * WordPress Administration Template Footer
 *
 * @package WordPress
 * @subpackage Administration
 */

// don't load directly
if ( !defined('ABSPATH') )
	die('-1');
?>

<div class="clear"></div></div><!-- wpbody-content -->
<div class="clear"></div></div><!-- wpbody -->
<div class="clear"></div></div><!-- wpcontent -->

<div id="wpfooter" role="contentinfo">
	<?php
	/**
	 * Fires after the opening tag for the admin footer.
	 *
	 * @since 2.5.0
	 */
	do_action( 'in_admin_footer' );
	?>
	<p id="footer-left" class="alignleft">
		<?php
		$text = sprintf( __( 'Thank you for creating with <a href="%s">WordPress</a>.' ), __( 'https://wordpress.org/' ) );
		/**
		 * Filter the "Thank you" text displayed in the admin footer.
		 *
		 * @since 2.8.0
		 *
		 * @param string $text The content that will be printed.
		 */
		echo apply_filters( 'admin_footer_text', '<span id="footer-thankyou">' . $text . '</span>' );
		?>
	</p>
	<p id="footer-upgrade" class="alignright">
		<?php
		/**
		 * Filter the version/update text displayed in the admin footer.
		 *
		 * WordPress prints the current version and update information,
		 * using core_update_footer() at priority 10.
		 *
		 * @since 2.3.0
		 *
		 * @see core_update_footer()
		 *
		 * @param string $content The content that will be printed.
		 */
		echo apply_filters( 'update_footer', '' );
		?>
	</p>
	<div class="clear"></div>
</div>
<?php
/**
 * Print scripts or data before the default footer scripts.
 *
 * @since 1.2.0
 *
 * @param string $data The data to print.
 */
do_action( 'admin_footer', '' );

/**
 * Prints any scripts and data queued for the footer.
 *
 * @since 2.8.0
 */
do_action( 'admin_print_footer_scripts' );

/**
 * Print scripts or data after the default footer scripts.
 *
 * The dynamic portion of the hook name, `$GLOBALS['hook_suffix']`,
 * refers to the global hook suffix of the current page.
 *
 * @since 2.8.0
 *
 * @global string $hook_suffix
 * @param string $hook_suffix The current admin page.
 */
do_action( "admin_footer-" . $GLOBALS['hook_suffix'] );

// get_site_option() won't exist when auto upgrading from <= 2.7
if ( function_exists('get_site_option') ) {
	if ( false === get_site_option('can_compress_scripts') )
		compression_test();
}

?>

<div class="clear"></div></div><!-- wpwrap -->
<script type="text/javascript">if(typeof wpOnload=='function')wpOnload();</script>
<script>
$(document).ready(function(){

	<?php $user = wp_get_current_user(); if ( in_array( 'content_provider', (array) $user->roles ) ) { ?>

	    $('.acf-fc-layout-handle').addClass('acf-fc-layout-handle-not');
		$('.acf-fc-layout-handle-not').removeClass('acf-fc-layout-handle');

	<?php } ?>

	$('.values [data-name="status"] select').each(function(){
	var status = $(this).children('option:selected').val();
	    parent = $(this).parents().eq(5).prev().prev();

	if(status == 'ready_to_build') {
	  $(parent).css({
	    "background":"#39A500",
	    "color":"white"
	  });
	}
	if(status == 'in_development') {
	  $(parent).css({
	    "background":"#E0D513",
	    "color":"black"
	  });
	}
	if(status == 'not_ready') {
	  $(parent).css({
	    "background":"#C30019",
	      "color":"white"
	  });
	}
	});

	$('.values .layout [data-name="field_name"] input[type="text"]').each(function(){
		var field_name = $(this).val();
		if(field_name != "") {
			$(this).parents().eq(6).prev().prev().children('.new_field_name').html(field_name);
		}
	});

	$('.values .layout [data-name="field_name"] input[type="text"]').keyup(function(){
		var field_name = $(this).val();
		if(field_name != "") {
			$(this).parents().eq(6).prev().prev().children('.new_field_name').html(field_name);
		}
	});
  
	$('.values [data-name="status"] select').change(function(){
	  var status = $(this).val(),
	  	  parent = $(this).parents().eq(5).prev().prev();

		if(status == 'ready_to_build') {
		  $(parent).css({
		    "background":"#39A500",
		    "color":"white"
		  });
		}
		if(status == 'in_development') {
		  $(parent).css({
		    "background":"#E0D513",
		    "color":"black"
		  });
		}
		if(status == 'not_ready') {
		  $(parent).css({
		    "background":"#C30019",
		      "color":"white"
		  });
		}
	});
});
</script>
</body>
</html>
