<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
// define('DB_NAME', 'thenodepole');

// /** MySQL database username */
// define('DB_USER', 'root');

// /** MySQL database password */
// define('DB_PASSWORD', 'root');

// /** MySQL hostname */
// define('DB_HOST', 'localhost');

// /** Database Charset to use in creating database tables. */
// define('DB_CHARSET', 'utf8mb4');

// /** The Database Collate type. Don't change this if in doubt. */
// define('DB_COLLATE', '');

define('DB_NAME', 'thenodepole');

/** MySQL-databasens användarnamn */
define('DB_USER', 'forge');

/** MySQL-databasens lösenord */
define('DB_PASSWORD', 'oiU2Wsn1OY45bbOLdU8w');

/** MySQL-server */
define('DB_HOST', '178.62.147.93');

/** Teckenkodning för tabellerna i databasen. */
define('DB_CHARSET', 'utf8mb4');

/** Kollationeringstyp för databasen. Ändra inte om du är osäker. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm/I]-WP_B/lzW$%NLI|s3RxIK`uvN]B{[uvCr5F%x<M%ZW+6-c*+?RQ1ZI9-WJWf');
define('SECURE_AUTH_KEY',  'K|<.i{SeFZ{K>.K-%o+CSe,d=bW0/5Apehjn2SE0|CA>EM]{v.+X7H`$3g )&wR&');
define('LOGGED_IN_KEY',    '}<aH^-#fD7Bq4sTLu%vkjJi#XcO+bL4=S+FUPpim)3m-+k^Qfli|v->C;EAi>+gi');
define('NONCE_KEY',        'lQ!@&>gu|ZUMPeD.EYY<c,[~CttrSI0A5^5kbExw&@q[G/<-z3IjACd3L[3{^Gvv');
define('AUTH_SALT',        '1LYa4{>CgQdugano=I.?mHw?NplCVn4lBPKUd30A1Qov80]bZKW|N668eGv.blpa');
define('SECURE_AUTH_SALT', '(!JAFqD]&m7sgI*U%D-@]r/+:3Ny|~w`K!r8-(vqM:dH%q/yK!2=_d=|6(87[g|l');
define('LOGGED_IN_SALT',   '91H}v^*(kxN9IV8J`4Pk/M0[$/rl%S^`z?C0mmQ6s:uAu*n&LVq#K.a[A%9xg~p)');
define('NONCE_SALT',       'rpK?;xnz,R|&5=/|w9J!5Tn_61%Y_b6R2sA2dK(|m $8,W* OCb#4e=h8L2</>zk');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'minplats_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
