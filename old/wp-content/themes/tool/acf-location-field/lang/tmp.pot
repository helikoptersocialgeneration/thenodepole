Field Order
Field Label
Field Name
Field Type
Drag and drop to reorder
+ Add Sub Field
Options
Flexible Content requires at least 1 layout
Label
Name
Display
Content
Location
Map address
Return the address along with the coordinates.
Yes
No
Map center
Latitude and longitude to center the initial map.
Map zoom
Map Scrollwheel
Allows scrollwheel zooming on the map field
Search for a location
Find the complete address
Address: 
Coordinates: 
Location Map
Return Value
Coordinates & Address
Coordinates
Map height
Height of the map. Minimum height is 150px
Latitude and longitude to center the initial map
Map Type Control
Street View Control
Show controls for Street View
Point Of Interest
Show places on the map
Post archives
Archive