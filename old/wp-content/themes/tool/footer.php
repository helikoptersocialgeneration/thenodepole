	<footer>

		<div class="container">

			<span>© 2011-2015 THE NODE POLE</span>

			<a href="mailto:info@thenodepole.com">info@thenodepole.com</a>

		</div>

	</footer>

    <script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/lightbox.min.js"></script>

  </body>

</html>