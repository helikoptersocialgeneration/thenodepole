
$(document).ready(function(){
    $(".toggle").click(function(){
        $(this).siblings(".info-row").slideToggle(200);
        $(this).toggleClass('active');
    });
});

$('a').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top -45
    }, 500);
    return false;
});