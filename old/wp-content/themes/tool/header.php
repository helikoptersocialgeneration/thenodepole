  <!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>The Node Pole <?php wp_title(); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/app.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/lightbox.css">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/images/favicon.ico">
    <?php wp_head(); ?>
  </head>

  <body>

  	<header>

      <div class="container">

    	   <img src="<?php bloginfo('template_url') ?>/images/nodepole_logo.png" class="logo">

      </div>

  	</header>