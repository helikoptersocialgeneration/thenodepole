module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      dist: {
        options: {
          outputStyle: 'compressed'
        },
        files: {
          'css/app.css': 'scss/app.scss'
        }        
      }
    },

    watch: {
      grunt: { files: ['Gruntfile.js'] },

      sass: {
        files: [
          'scss/**/*.scss', 'js/app.js'
          ],
        tasks: ['sass', 'uglify']
      }
    },

    uglify: {
      options: {  
        compress: true  
      },
      applib: {  
        src: [  
          'js/app.js'
        ],
        dest: 'js/app.min.js'
      }
    }
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('build', ['sass']);
  grunt.registerTask('default', ['build','watch','uglify']);
} 