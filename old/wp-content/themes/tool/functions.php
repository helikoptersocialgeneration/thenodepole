<?php

// Include advanced custom fields
include_once('advanced-custom-fields/acf.php' );
include_once('acf-options-page/acf-options-page.php');
include_once('acf-location-field/acf-location.php');

// Include advanced custom fields repeater fields
function tsg_repeater_fields() {
    include_once('acf-repeater/repeater.php');
    include_once('acf-flexible-content/flexible-content.php');
}
add_action('acf/register_fields', 'tsg_repeater_fields');

// Disable WordPress update notifications
add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;" ) );

// Advanced custom fields settings
if(function_exists("register_options_page"))
{
    // register_options_page('Duplicate');
}

// Change option page titlel
function tsg_option_page_settings($options) {
    $options['title'] = 'Duplicate';
    return $options;
}
add_filter('acf/options_page/settings', 'tsg_option_page_settings');


// Hide menu from others the admin
function tsg_remove_acf_menu() {
 
    $admins = array( 
        'patrik'
    );
 
    $current_user = wp_get_current_user();
 
    if( !in_array( $current_user->user_login, $admins ) )
    {
        remove_menu_page('edit.php?post_type=acf');
        remove_submenu_page('index.php', 'update-core.php');
    }
 
}
//add_action( 'admin_menu', 'tsg_remove_acf_menu' );

if ( !is_admin() )
{
    global $sitepress;
    remove_action( 'wp_head', array( $sitepress, 'meta_generator_tag', 20 ) );
}

// Remove comments
function tsg_remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
add_action('init', 'tsg_remove_comment_support', 100);

// Remove tags from posts
function tsg_remove_tags() {
    global $wp_taxonomies;
    unset( $wp_taxonomies['post_tag'] );
}
add_action( 'init', 'tsg_remove_tags' );

// Remove admin toolbar options
function tsg_remove_admin_bar_links() {
    global $wp_admin_bar;
    // $wp_admin_bar->remove_menu('about'); // Remove the about WordPress link
    // $wp_admin_bar->remove_menu('wporg'); // Remove the WordPress.org link
    // $wp_admin_bar->remove_menu('documentation'); // Remove the WordPress documentation link
    // $wp_admin_bar->remove_menu('support-forums'); // Remove the support forums link
    // $wp_admin_bar->remove_menu('feedback'); // Remove the feedback link
    // $wp_admin_bar->remove_menu('site-name'); // Remove the site name menu
    // $wp_admin_bar->remove_menu('view-site'); // Remove the view site link
    // $wp_admin_bar->remove_menu('w3tc'); // If you use w3 total cache remove the performance link
    // $wp_admin_bar->remove_menu('my-account'); // Remove the user details tab
    $wp_admin_bar->remove_menu('wp-logo'); // Remove the WordPress logo
    $wp_admin_bar->remove_menu('updates'); // Remove the updates link
    $wp_admin_bar->remove_menu('comments'); // Remove the comments link
    $wp_admin_bar->remove_menu('new-content'); // Remove the content link
    $wp_admin_bar->remove_menu('wpseo-menu');
}
add_action( 'wp_before_admin_bar_render', 'tsg_remove_admin_bar_links' );

function my_remove_menu_elements()
{    
    remove_submenu_page( 'themes.php', 'themes.php' );
    remove_submenu_page( 'themes.php', 'theme-editor.php' );
    remove_submenu_page( 'themes.php', 'customize.php' );
}
add_action('admin_menu', 'my_remove_menu_elements', 102);

add_action( 'init', 'cptui_register_my_cpts' );
function cptui_register_my_cpts() {
    $labels = array(
        "name" => "Sites",
        "singular_name" => "Site",
        "menu_name" => "Sites",
        "all_items" => "All Sites",
        "add_new" => "Add New",
        "add_new_item" => "Add New Site",
        "edit" => "Edit",
        "edit_item" => "Edit Site",
        "new_item" => "New Site",
        "view" => "View",
        "view_item" => "View Site",
        "search_items" => "Search Site",
        "not_found" => "No Sites found",
        "not_found_in_trash" => "No Sites found in Trash",
        "parent" => "Parent Site",
        );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "sites", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "revisions" ),
    );
    register_post_type( "sites", $args );

// End of cptui_register_my_cpts()
}

// Remove dashboard widgets
function tsg_remove_dashboard_widgets() {
    remove_action( 'welcome_panel', 'wp_welcome_panel' );
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'advanced' );
    remove_meta_box( 'icl_dashboard_widget', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
}
//add_action('admin_init', 'tsg_remove_dashboard_widgets');

add_filter( 'wpseo_metabox_prio', function() { return 'low';});

// Add custom css to "body_class" for WPML / custom stuff
function tsg_custom_bodyclass($classes) {
    global $wp_query, $post;

    $types = array('products');
    $type = get_post_type( $post );
    $page_id = $post->ID;
    $page_parent_id = $post->post_parent;
    $pid = $page_parent_id >= 1 ? $page_parent_id : $page_id;

    $submenu_class = isset($wp_query->post_count) && $wp_query->post_count >= 2 && !is_home() && $type != 'ice-products' ? 'page-has-parent' : 'page-has-no-parent';
    $submenu_class = wp_list_pages('title_li=&child_of='.$pid.'&echo=0') ? 'page-has-parent': $submenu_class;
    $submenu_class = in_array($type , $types) ? 'page-has-parent': $submenu_class;

    $submenu_class = is_category() ? 'page-has-no-parent' : $submenu_class;


    $classes[] = $submenu_class;

    return $classes;
}
// add_filter('body_class','tsg_custom_bodyclass');


// Remove admin menu links
function tsg_remove_admin_menus(){
    global $menu, $current_user;
    if ( $current_user->ID == 4 )
    {
        $restricted = array(__('Comments'), __('Tools'), __('Plugins'), __('Settings'));
    }
    else
    {
        $restricted = array(__('Comments'));
    }
    end ($menu);
    while (prev($menu)) {
        $value = explode(' ',$menu[key($menu)][0]);
        if ( in_array($value[0] != NULL?$value[0]:"" , $restricted))
        {
            unset($menu[key($menu)]);
        }
    }
}
add_action('admin_menu', 'tsg_remove_admin_menus');

// Hide ACF menu
function remove_acf_menu(){
    remove_menu_page( 'edit.php?post_type=acf' );
    remove_menu_page( 'wpseo_dashboard' );
    remove_menu_page( 'sitepress-multilingual-cms/menu/languages.php' );
    remove_menu_page( 'edit.php?post_type=page' ); 
    remove_menu_page( 'edit.php' );
    remove_menu_page( 'themes.php' ); 
    remove_menu_page( 'plugins.php' );
    remove_menu_page( 'tools.php' );
    remove_menu_page( 'options-general.php' );
    remove_menu_page( 'index.php' );
    remove_menu_page('acf-options');
}
add_action( 'admin_menu', 'remove_acf_menu', 999 );

// Remove admin bar
function tsg_remove_admin_bar() {
    show_admin_bar(false);
}
add_action('after_setup_theme', 'tsg_remove_admin_bar');


// Register wordpress nav menus and other init stuff
function tsg_register_menu() {
    register_nav_menu( 'navigation', 'Navigering' );
    remove_filter("wp_head", "wp_generator");
    // register_nav_menu( 'sidfot', 'Meny i sidfoot' );
}
add_action( 'init', 'tsg_register_menu' );


add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );

/* Don't include migrate */
function dequeue_jquery_migrate( &$scripts){
    if(!is_admin()){
        $scripts->remove( 'jquery');
        //$scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
    }
}

//Hide pages from WordPress admin
function tsg_exclude_pages_from_admin($query) {
 
  if ( ! is_admin() )
    return $query;
 
  global $pagenow, $post_type, $current_user;
 
  if ( ($current_user->ID != 1) && $pagenow == 'edit.php' && $post_type == 'page' )
    $query->query_vars['post__not_in'] = array( '183' ); // Enter your page IDs here
  
 
}
//add_filter( 'parse_query', 'tsg_exclude_pages_from_admin' );

function textBlockTopicLeftContentRight() {
    include 'elements/textBlockTopicLeftContentRight.php';
}

function portfolioBlock() {
    include 'elements/portfolioBlock.php';
}

function partners($limit = null) {    
    include 'elements/partners.php';
}

function twoColumnTextBlocks() {
    include 'elements/twoColumnTextBlocks.php';
}

function socialIcons() {
    include 'elements/socialIcons.php';
}

function helikopter_paging_nav($permalink) {
    global $wp_query;

    $currentPage = intval(get_query_var('paged'));

    // Don't print empty markup if there's only one page.
    if ( $wp_query->max_num_pages < 2 )
        return;
    ?>

    <nav class="navigation paging-navigation" role="navigation">        
        <div class="row pagination">
            <div class="small-12 columns">
                <?php if ( get_previous_posts_link() ) : ?>
                    <div class="prevPage"><?php previous_posts_link( 'Föregående' ); ?></div>
                <?php endif; ?>

                <div class="pageNumbers textAlignCenter">
                    <?php for($i = 1; $i <= $wp_query->max_num_pages; $i++): ?>
                        <a href="<?php echo $permalink; ?>page/<?php echo $i; ?>" <?php if($i === $currentPage) echo 'class="active-page"'; ?>><?php echo $i; ?></a>
                    <?php endfor; ?>
                </div>            

                <?php if ( get_next_posts_link() ) : ?>
                    <div class="nextPage"><?php next_posts_link( 'Nästa' ); ?></div>
                <?php endif; ?>
            </div>
        </div><!-- .nav-links -->
    </nav><!-- .navigation -->
    <?php
}



if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_site-information-fields',
        'title' => 'Site Information Fields',
        'fields' => array (
            array (
                'key' => 'field_5548a85a51c48',
                'label' => 'Site Fields',
                'name' => 'site_fields',
                'type' => 'repeater',
                'sub_fields' => array (
                    array (
                        'key' => 'field_5548a98f59e63',
                        'label' => 'Site Section Name',
                        'name' => 'site_section_name',
                        'type' => 'text',
                        'column_width' => 20,
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'formatting' => 'html',
                        'maxlength' => '',
                    ),
                    array (
                        'key' => 'field_5548a87651c49',
                        'label' => 'Site Section Fields',
                        'name' => 'site_section_fields',
                        'type' => 'flexible_content',
                        'column_width' => 80,
                        'layouts' => array (
                            array (
                                'label' => 'Text',
                                'name' => 'text',
                                'display' => 'row',
                                'min' => '',
                                'max' => '',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_5548ae7972184',
                                        'label' => 'Field Name',
                                        'name' => 'field_name',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548ae8272185',
                                        'label' => 'Text',
                                        'name' => 'text',
                                        'type' => 'textarea',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'formatting' => 'br',
                                    ),
                                    array (
                                        'key' => 'field_5548b262ffd3c',
                                        'label' => 'Deadline',
                                        'name' => 'deadline',
                                        'type' => 'date_picker',
                                        'column_width' => '',
                                        'date_format' => 'yymmdd',
                                        'display_format' => 'dd/mm/yy',
                                        'first_day' => 1,
                                    ),
                                    array (
                                        'key' => 'field_5548b1be1a495',
                                        'label' => 'Person Responsible',
                                        'name' => 'person_responsible',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548b1ca1a496',
                                        'label' => 'Source',
                                        'name' => 'source',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548ae9b72186',
                                        'label' => 'Comment',
                                        'name' => 'comment',
                                        'type' => 'textarea',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'formatting' => 'br',
                                    ),
                                    array (
                                        'key' => 'field_5548ac9bd604f',
                                        'label' => 'Add to site summary',
                                        'name' => 'summary',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'No' => 'No',
                                            'land' => 'Land (sq)',
                                            'power-available' => 'Power available',
                                            'power-available-in-12-months' => 'Power available in 12 months',
                                            'power-available-in-18-months' => 'Power available in 18 months',
                                            'power-available-in-24-months' => 'Power available in 24 months',
                                            'power-available-in-36-months' => 'Power available in 36 months',
                                            'water-available' => 'Water available',
                                            'sewer-available' => 'Sewer available',                                            
                                        ),
                                        'default_value' => 'no',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),                                
                                    array (
                                        'key' => 'field_5548c1a8e244d',
                                        'label' => 'Status',
                                        'name' => 'status',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'ready_to_build' => 'Ready To Build',
                                            'in_development' => 'In Development',
                                            'not_ready' => 'Not Ready',
                                        ),
                                        'default_value' => 'not_ready',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5548ac9bd6bbb',
                                        'label' => 'Level to view',
                                        'name' => 'leveltoview',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'level1' => 'Level 1',
                                            'level2' => 'Level 2',
                                            'level3' => 'Level 3',                                           
                                        ),
                                        'default_value' => 'no',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),    
                                ),
                            ),
                            array (
                                'label' => 'Value',
                                'name' => 'value',
                                'display' => 'row',
                                'min' => '',
                                'max' => '',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_5548adc5faabc',
                                        'label' => 'Field Name',
                                        'name' => 'field_name',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548abfc93a3c',
                                        'label' => 'Value',
                                        'name' => 'value',
                                        'type' => 'number',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'min' => '',
                                        'max' => '',
                                        'step' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548ac9bd604e',
                                        'label' => 'Unit',
                                        'name' => 'unit',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'krona' => 'kr',
                                            'dollar' => '$',
                                            'pounds' => '£',
                                            'euro' => '€',
                                            'm2' => 'm²',
                                            'meter' => 'meter',
                                        ),
                                        'default_value' => 'krona',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5548b278ffd3e',
                                        'label' => 'Deadline',
                                        'name' => 'deadline',
                                        'type' => 'date_picker',
                                        'column_width' => '',
                                        'date_format' => 'yymmdd',
                                        'display_format' => 'dd/mm/yy',
                                        'first_day' => 1,
                                    ),
                                    array (
                                        'key' => 'field_5548b218b01b5',
                                        'label' => 'Person Responsible',
                                        'name' => 'person_responsible',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548b21eb01b6',
                                        'label' => 'Source',
                                        'name' => 'source',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548adcbfaabd',
                                        'label' => 'Comment',
                                        'name' => 'comment',
                                        'type' => 'textarea',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'formatting' => 'br',
                                    ),
                                    array (
                                        'key' => 'field_5548ac9bd604f',
                                        'label' => 'Add to site summary',
                                        'name' => 'summary',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'no' => 'No',                                            
                                            'land' => 'Land (sq)',
                                            'power-available' => 'Power available',
                                            'power-available-in-12-months' => 'Power available in 12 months',
                                            'power-available-in-18-months' => 'Power available in 18 months',
                                            'power-available-in-24-months' => 'Power available in 24 months',
                                            'power-available-in-36-months' => 'Power available in 36 months',
                                            'water-available' => 'Water available',
                                            'sewer-available' => 'Sewer available',                                            
                                        ),
                                        'default_value' => 'no',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),                                      
                                    array (
                                        'key' => 'field_5548c0d57da1c',
                                        'label' => 'Status',
                                        'name' => 'status',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'ready_to_build' => 'Ready To Build',
                                            'in_development' => 'In Development',
                                            'not_ready' => 'Not Ready',
                                        ),
                                        'default_value' => 'not_ready',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5548ac9bd6ccc',
                                        'label' => 'Level to view',
                                        'name' => 'leveltoview',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'level1' => 'Level 1',
                                            'level2' => 'Level 2',
                                            'level3' => 'Level 3',                                           
                                        ),
                                        'default_value' => 'no',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),  
                                ),
                            ),
                            array (
                                'label' => 'Image',
                                'name' => 'image',
                                'display' => 'row',
                                'min' => '',
                                'max' => '',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_5548a8a851c4a',
                                        'label' => 'Field Name',
                                        'name' => 'field_name',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548a8bb51c4b',
                                        'label' => 'Image',
                                        'name' => 'image',
                                        'type' => 'image',
                                        'column_width' => '',
                                        'save_format' => 'object',
                                        'preview_size' => 'thumbnail',
                                        'library' => 'all',
                                    ),
                                    array (
                                        'key' => 'field_5548b290ffd3f',
                                        'label' => 'Deadline',
                                        'name' => 'deadline',
                                        'type' => 'date_picker',
                                        'column_width' => '',
                                        'date_format' => 'yymmdd',
                                        'display_format' => 'dd/mm/yy',
                                        'first_day' => 1,
                                    ),
                                    array (
                                        'key' => 'field_5548b056f48dc',
                                        'label' => 'Person Responsible',
                                        'name' => 'person_responsible',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548b060f48dd',
                                        'label' => 'Source',
                                        'name' => 'source',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548a8ca51c4c',
                                        'label' => 'Comment',
                                        'name' => 'comment',
                                        'type' => 'textarea',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'formatting' => 'br',
                                    ),
                                    array (
                                        'key' => 'field_5548b039f48db',
                                        'label' => 'Status',
                                        'name' => 'status',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'ready_to_build' => 'Ready To Build',
                                            'in_development' => 'In Development',
                                            'not_ready' => 'Not Ready',
                                        ),
                                        'default_value' => 'not_ready',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5548ac9bd6ddd',
                                        'label' => 'Level to view',
                                        'name' => 'leveltoview',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'level1' => 'Level 1',
                                            'level2' => 'Level 2',
                                            'level3' => 'Level 3',                                           
                                        ),
                                        'default_value' => 'no',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),    
                                ),
                            ),
                            array (
                                'label' => 'Gallery',
                                'name' => 'gallery',
                                'display' => 'row',
                                'min' => '',
                                'max' => '',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_5548b20eb01b4',
                                        'label' => 'Field Name',
                                        'name' => 'field_name',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548adf9faabf',
                                        'label' => 'Images',
                                        'name' => 'images',
                                        'type' => 'repeater',
                                        'column_width' => '',
                                        'sub_fields' => array (
                                            array (
                                                'key' => 'field_5548ae1cfaac1',
                                                'label' => 'Title',
                                                'name' => 'title',
                                                'type' => 'text',
                                                'column_width' => '',
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'prepend' => '',
                                                'append' => '',
                                                'formatting' => 'html',
                                                'maxlength' => '',
                                            ),
                                            array (
                                                'key' => 'field_5548ae12faac0',
                                                'label' => 'Image',
                                                'name' => 'image',
                                                'type' => 'image',
                                                'column_width' => '',
                                                'save_format' => 'object',
                                                'preview_size' => 'thumbnail',
                                                'library' => 'all',
                                            ),
                                            array (
                                                'key' => 'field_5548ae24faac2',
                                                'label' => 'Comment',
                                                'name' => 'comment',
                                                'type' => 'textarea',
                                                'column_width' => '',
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'maxlength' => '',
                                                'formatting' => 'br',
                                            ),
                                        ),
                                        'row_min' => '',
                                        'row_limit' => '',
                                        'layout' => 'table',
                                        'button_label' => 'Add New Image',
                                    ),
                                    array (
                                        'key' => 'field_5548b26fffd3d',
                                        'label' => 'Deadline',
                                        'name' => 'deadline',
                                        'type' => 'date_picker',
                                        'column_width' => '',
                                        'date_format' => 'yymmdd',
                                        'display_format' => 'dd/mm/yy',
                                        'first_day' => 1,
                                    ),
                                    array (
                                        'key' => 'field_5548b1e71a498',
                                        'label' => 'Person Responsible',
                                        'name' => 'person_responsible',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548b1f21a499',
                                        'label' => 'Source',
                                        'name' => 'source',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548b1f51a49a',
                                        'label' => 'Comment',
                                        'name' => 'comment',
                                        'type' => 'textarea',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'formatting' => 'br',
                                    ),
                                    array (
                                        'key' => 'field_5548c10a7da1d',
                                        'label' => 'Status',
                                        'name' => 'status',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'ready_to_build' => 'Ready To Build',
                                            'in_development' => 'In Development',
                                            'not_ready' => 'Not Ready',
                                        ),
                                        'default_value' => 'not_ready',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5548ac9bd6eee',
                                        'label' => 'Level to view',
                                        'name' => 'leveltoview',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'level1' => 'Level 1',
                                            'level2' => 'Level 2',
                                            'level3' => 'Level 3',                                           
                                        ),
                                        'default_value' => 'no',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),    
                                ),
                            ),
                            array (
                                'label' => 'Completion Status',
                                'name' => 'status',
                                'display' => 'row',
                                'min' => '',
                                'max' => '',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_5548af617218b',
                                        'label' => 'Field Name',
                                        'name' => 'field_name',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548aebe72188',
                                        'label' => 'Status',
                                        'name' => 'status',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'completed' => 'Completed',
                                            'not_completed' => 'Not Completed',
                                        ),
                                        'default_value' => 'not_completed : Not Completed',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5548af2372189',
                                        'label' => 'When Completed?',
                                        'name' => 'when_completed?',
                                        'type' => 'date_picker',
                                        'conditional_logic' => array (
                                            'status' => 1,
                                            'rules' => array (
                                                array (
                                                    'field' => 'field_5548aebe72188',
                                                    'operator' => '==',
                                                    'value' => 'not_completed',
                                                ),
                                            ),
                                            'allorany' => 'all',
                                        ),
                                        'column_width' => '',
                                        'date_format' => 'yymmdd',
                                        'display_format' => 'dd/mm/yy',
                                        'first_day' => 1,
                                    ),
                                    array (
                                        'key' => 'field_5548b259ffd3b',
                                        'label' => 'Deadline',
                                        'name' => 'deadline',
                                        'type' => 'date_picker',
                                        'column_width' => '',
                                        'date_format' => 'yymmdd',
                                        'display_format' => 'dd/mm/yy',
                                        'first_day' => 1,
                                    ),
                                    array (
                                        'key' => 'field_5548b13ef48df',
                                        'label' => 'Person Responsible',
                                        'name' => 'person_responsible',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548b159f48e1',
                                        'label' => 'Source',
                                        'name' => 'source',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548b152f48e0',
                                        'label' => 'Comment',
                                        'name' => 'comment',
                                        'type' => 'textarea',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'formatting' => 'br',
                                    ),
                                    array (
                                        'key' => 'field_5548c1187da1e',
                                        'label' => 'Status',
                                        'name' => 'status',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'ready_to_build' => 'Ready To Build',
                                            'in_development' => 'In Development',
                                            'not_ready' => 'Not Ready',
                                        ),
                                        'default_value' => 'not_ready',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5548ac9bd6fff',
                                        'label' => 'Level to view',
                                        'name' => 'leveltoview',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'level1' => 'Level 1',
                                            'level2' => 'Level 2',
                                            'level3' => 'Level 3',                                           
                                        ),
                                        'default_value' => 'no',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),    
                                ),
                            ),
                            array (
                                'label' => 'Yes or No',
                                'name' => 'yes_or_no',
                                'display' => 'row',
                                'min' => '',
                                'max' => '',
                                'sub_fields' => array (
                                    array (
                                        'key' => 'field_5548afc2f48da',
                                        'label' => 'Field Name',
                                        'name' => 'field_name',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548af91f48d9',
                                        'label' => 'Yes?',
                                        'name' => 'yes',
                                        'type' => 'true_false',
                                        'column_width' => '',
                                        'message' => '',
                                        'default_value' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5548b239ffd3a',
                                        'label' => 'Deadline',
                                        'name' => 'deadline',
                                        'type' => 'date_picker',
                                        'column_width' => '',
                                        'date_format' => 'yymmdd',
                                        'display_format' => 'dd/mm/yy',
                                        'first_day' => 1,
                                    ),
                                    array (
                                        'key' => 'field_5548b17ff48e2',
                                        'label' => 'Person Responsible',
                                        'name' => 'person_responsible',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548b188f48e3',
                                        'label' => 'Source',
                                        'name' => 'source',
                                        'type' => 'text',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'prepend' => '',
                                        'append' => '',
                                        'formatting' => 'html',
                                        'maxlength' => '',
                                    ),
                                    array (
                                        'key' => 'field_5548b18df48e4',
                                        'label' => 'Comment',
                                        'name' => 'comment',
                                        'type' => 'textarea',
                                        'column_width' => '',
                                        'default_value' => '',
                                        'placeholder' => '',
                                        'maxlength' => '',
                                        'formatting' => 'br',
                                    ),
                                    array (
                                        'key' => 'field_5548c1267da1f',
                                        'label' => 'Status',
                                        'name' => 'status',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'ready_to_build' => 'Ready To Build',
                                            'in_development' => 'In Development',
                                            'not_ready' => 'Not Ready',
                                        ),
                                        'default_value' => 'not_ready',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),
                                    array (
                                        'key' => 'field_5548ac9bd6ggg',
                                        'label' => 'Level to view',
                                        'name' => 'leveltoview',
                                        'type' => 'select',
                                        'column_width' => '',
                                        'choices' => array (
                                            'level1' => 'Level 1',
                                            'level2' => 'Level 2',
                                            'level3' => 'Level 3',                                           
                                        ),
                                        'default_value' => 'no',
                                        'allow_null' => 0,
                                        'multiple' => 0,
                                    ),    
                                ),
                            ),
                        ),
                        'button_label' => 'Add field',
                        'min' => '',
                        'max' => '',
                    ),
                ),
                'row_min' => '',
                'row_limit' => '',
                'layout' => 'table',
                'button_label' => 'Add field section',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'sites',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
                0 => 'permalink',
                1 => 'the_content',
                2 => 'excerpt',
                3 => 'custom_fields',
                4 => 'discussion',
                5 => 'comments',
                6 => 'slug',
                7 => 'author',
                8 => 'format',
                9 => 'featured_image',
                10 => 'categories',
                11 => 'tags',
                12 => 'send-trackbacks',
            ),
        ),
        'menu_order' => 0,
    ));
}

// Sites functions


// Site Status
function siteStatus($field_name, $letters, $status) {

	$the_field_name = $field_name;
	$field_name = str_replace($letters, "", $field_name);

	if ($status == 'not_ready') {

		return '<a href="#'. $field_name .'" class="not-ready" title="'. $the_field_name .'"></a>';

	} elseif ($status == 'in_development') {

		return '<a href="#'. $field_name .'" class="in-development" title="'. $the_field_name .'"></a>';					

	} elseif ($status == 'ready_to_build') {

		return '<a href="#'. $field_name . '" class="ready-to-build" title="'. $the_field_name .'"></a>';
	
	}

}


// Text Layout
function textLayout($field_name, $text, $deadline, $status, $person_responsible, $source, $comment) {

	$letters = array("?", ".", "!", "(", ")", " ");
	$field_name_link = str_replace($letters, "", $field_name);

	if($deadline) {
		$deadline = date("Y-m-d", strtotime($deadline));
	}

	if ($status == 'not_ready') {
		$statusclass = 'not-ready';
	} elseif ($status == 'in_development') {
		$statusclass = 'in-development';
	} elseif ($status == 'ready_to_build') {
		$statusclass = 'ready-to-build';
	}

	return '<div class="field '. $statusclass .'">

		<div class="toggle">
		<svg width="18px" height="12px" viewBox="0 0 18 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	    		<g transform="translate(-881.000000, -883.000000)" fill="#9B9B9B">
	        		<g transform="translate(520.000000, 863.000000)">
	            		<path d="M371.723788,26.0001747 L364.579073,18.8554594 L366.430191,17.0043406 L375.412246,26.0286957 L373.582278,27.8586643 L373.574907,27.8512935 L366.430541,34.9956594 L364.579422,33.1445406 L371.723788,26.0001747 Z" transform="translate(369.995659, 26.000000) rotate(-270.000000) translate(-369.995659, -26.000000) "></path>
	        		</g>
	    		</g>
			</g>
		</svg>
		</div>

		<h2 id="'. $field_name_link .'">'. $field_name .'</h2>
		<div class="info-row">
			<div class="fourth">
				<h6>Text</h6>
				<span>'. $text .'</span>
			</div>
			<div class="fourth">
				<h6>Deadline</h6>
				<span>'. $deadline .'</span>
			</div>
			<div class="fourth">
				<h6>Person responsible</h6>
				<span>'. $person_responsible .'</span>
			</div>
			<div class="fourth">
				<h6>Source</h6>
				<span>'. $source .'</span>
			</div>
			<div class="full">
				<h6>Comment</h6>
				<span>'. $comment .'</span>
			</div>
		</div>									
	</div>';

}


// Value Layout
function valueLayout($field_name, $value, $deadline, $status, $person_responsible, $source, $comment, $label) {

	$letters = array("?", ".", "!", "(", ")", " ");
	$field_name_link = str_replace($letters, "", $field_name);


	if($deadline) {
		$deadline = date("Y-m-d", strtotime($deadline));
	}

	if ($status == 'not_ready') {
		$statusclass = 'not-ready';
	} elseif ($status == 'in_development') {
		$statusclass = 'in-development';
	} elseif ($status == 'ready_to_build') {
		$statusclass = 'ready-to-build';
	}

	return '<div class="field '. $statusclass .'">

		<div class="toggle">
		<svg width="18px" height="12px" viewBox="0 0 18 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	    		<g transform="translate(-881.000000, -883.000000)" fill="#9B9B9B">
	        		<g transform="translate(520.000000, 863.000000)">
	            		<path d="M371.723788,26.0001747 L364.579073,18.8554594 L366.430191,17.0043406 L375.412246,26.0286957 L373.582278,27.8586643 L373.574907,27.8512935 L366.430541,34.9956594 L364.579422,33.1445406 L371.723788,26.0001747 Z" transform="translate(369.995659, 26.000000) rotate(-270.000000) translate(-369.995659, -26.000000) "></path>
	        		</g>
	    		</g>
			</g>
		</svg>
		</div>

		<h2 id="'. $field_name_link .'">'. $field_name .'</h2>
		<div class="info-row">
			<div class="fourth">
				<h6>Value</h6>
				<span>'. $value .' '. $label .'</span>
			</div>
			<div class="fourth">
				<h6>Deadline</h6>
				<span>'. $deadline .'</span>
			</div>
			<div class="fourth">
				<h6>Person responsible</h6>
				<span>'. $person_responsible .'</span>
			</div>
			<div class="fourth">
				<h6>Source</h6>
				<span>'. $source .'</span>
			</div>
			<div class="full">
				<h6>Comment</h6>
				<span>'. $comment .'</span>
			</div>
		</div>									
	</div>';

}

// Yes and No Layout
function yesnoLayout($field_name, $yesno, $deadline, $status, $person_responsible, $source, $comment) {

	$letters = array("?", ".", "!", "(", ")", " ");
	$field_name_link = str_replace($letters, "", $field_name);

	if($deadline) {
		$deadline = date("Y-m-d", strtotime($deadline));
	}

	if ($status == 'not_ready') {
		$statusclass = 'not-ready';
	} elseif ($status == 'in_development') {
		$statusclass = 'in-development';
	} elseif ($status == 'ready_to_build') {
		$statusclass = 'ready-to-build';
	}

	return '<div class="field '. $statusclass .'">

		<div class="toggle">
		<svg width="18px" height="12px" viewBox="0 0 18 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	    		<g transform="translate(-881.000000, -883.000000)" fill="#9B9B9B">
	        		<g transform="translate(520.000000, 863.000000)">
	            		<path d="M371.723788,26.0001747 L364.579073,18.8554594 L366.430191,17.0043406 L375.412246,26.0286957 L373.582278,27.8586643 L373.574907,27.8512935 L366.430541,34.9956594 L364.579422,33.1445406 L371.723788,26.0001747 Z" transform="translate(369.995659, 26.000000) rotate(-270.000000) translate(-369.995659, -26.000000) "></path>
	        		</g>
	    		</g>
			</g>
		</svg>
		</div>

		<h2 id="'. $field_name_link .'">'. $field_name .'</h2>
		<div class="info-row">
			<div class="fourth">
				<h6>Yes / No</h6>
				<span>'. $yesno .'</span>
			</div>
			<div class="fourth">
				<h6>Deadline</h6>
				<span>'. $deadline .'</span>
			</div>
			<div class="fourth">
				<h6>Person responsible</h6>
				<span>'. $person_responsible .'</span>
			</div>
			<div class="fourth">
				<h6>Source</h6>
				<span>'. $source .'</span>
			</div>
			<div class="full">
				<h6>Comment</h6>
				<span>'. $comment .'</span>
			</div>
		</div>									
	</div>';

}

// Image Layout
function imageLayout($field_name, $image, $deadline, $status, $person_responsible, $source, $comment) {

	$letters = array("?", ".", "!", "(", ")", " ");
	$field_name_link = str_replace($letters, "", $field_name);

	if($deadline) {
		$deadline = date("Y-m-d", strtotime($deadline));
	}

	if ($status == 'not_ready') {
		$statusclass = 'not-ready';
	} elseif ($status == 'in_development') {
		$statusclass = 'in-development';
	} elseif ($status == 'ready_to_build') {
		$statusclass = 'ready-to-build';
	}

	return '<div class="field '. $statusclass .'">

		<div class="toggle">
		<svg width="18px" height="12px" viewBox="0 0 18 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	    		<g transform="translate(-881.000000, -883.000000)" fill="#9B9B9B">
	        		<g transform="translate(520.000000, 863.000000)">
	            		<path d="M371.723788,26.0001747 L364.579073,18.8554594 L366.430191,17.0043406 L375.412246,26.0286957 L373.582278,27.8586643 L373.574907,27.8512935 L366.430541,34.9956594 L364.579422,33.1445406 L371.723788,26.0001747 Z" transform="translate(369.995659, 26.000000) rotate(-270.000000) translate(-369.995659, -26.000000) "></path>
	        		</g>
	    		</g>
			</g>
		</svg>
		</div>

		<h2 id="'. $field_name_link .'">'. $field_name .'</h2>
		<div class="info-row">
			<div class="fourth">
				<h6>Image</h6>
				<a data-lightbox="Image" href="'. $image['url'] . '">
					<img src="'. $image['sizes']['thumbnail'] .'" alt="'. $image['alt'] .'" title="'. $image['alt'] .'">
				</a>
			</div>
			<div class="fourth">
				<h6>Deadline</h6>
				<span>'. $deadline .'</span>
			</div>
			<div class="fourth">
				<h6>Person responsible</h6>
				<span>'. $person_responsible .'</span>
			</div>
			<div class="fourth">
				<h6>Source</h6>
				<span>'. $source .'</span>
			</div>
			<div class="full">
				<h6>Comment</h6>
				<span>'. $comment .'</span>
			</div>
		</div>									
	</div>';

}


// Gallery Layout
function galleryLayout($field_name, $images, $deadline, $status, $person_responsible, $source, $comment) {

	$letters = array("?", ".", "!", "(", ")", " ");
	$field_name_link = str_replace($letters, "", $field_name);

	if($deadline) {
		$deadline = date("Y-m-d", strtotime($deadline));
	}

	if ($status == 'not_ready') {
		$statusclass = 'not-ready';
	} elseif ($status == 'in_development') {
		$statusclass = 'in-development';
	} elseif ($status == 'ready_to_build') {
		$statusclass = 'ready-to-build';
	}

	return '<div class="field '. $statusclass .'">

		<div class="toggle">
		<svg width="18px" height="12px" viewBox="0 0 18 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	    		<g transform="translate(-881.000000, -883.000000)" fill="#9B9B9B">
	        		<g transform="translate(520.000000, 863.000000)">
	            		<path d="M371.723788,26.0001747 L364.579073,18.8554594 L366.430191,17.0043406 L375.412246,26.0286957 L373.582278,27.8586643 L373.574907,27.8512935 L366.430541,34.9956594 L364.579422,33.1445406 L371.723788,26.0001747 Z" transform="translate(369.995659, 26.000000) rotate(-270.000000) translate(-369.995659, -26.000000) "></path>
	        		</g>
	    		</g>
			</g>
		</svg>
		</div>

		<h2 id="'. $field_name_link .'">'. $field_name .'</h2>
		<div class="info-row">
			<div class="full gallery">
				<h6>Gallery</h6>					
			    <ul>
			        '. images($images) .'
			    </ul>
		    </div>
			<div class="fourth">
				<h6>Deadline</h6>
				<span>'. $deadline .'</span>
			</div>
			<div class="fourth">
				<h6>Person responsible</h6>
				<span>'. $person_responsible .'</span>
			</div>
			<div class="fourth">
				<h6>Source</h6>
				<span>'. $source .'</span>
			</div>
			<div class="full">
				<h6>Comment</h6>
				<span>'. $comment .'</span>
			</div>
		</div>									
	</div>';

}


// Images function - Gallery Layout
function images($images) {
	$gallery = "";

	foreach( $images as $image ) {
        $gallery .= '<li>
			<a data-lightbox="Image" href="'. $image['image']['url'] .'">
				<img src="'. $image['image']['sizes']['thumbnail'] .'" alt="'. $image['image']['alt'] .'" title="'. $image['image']['alt'] .'">
			</a>
        </li>';
     }

     return $gallery;
}

?>