<?php get_header(); ?>

<?php if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>

<div class="container">

<?php

	// getting the current logged in user-role
	$user_roles = $current_user->roles;
	$user_role = array_shift($user_roles);
?>

	<section class="site-summary">

		<h1>Site summary</h1>

		<div class="half">

			<div class="field">

				<h6 class="half">Land (sq meters)</h6>

				<?php if( have_rows('site_fields') ):

				    while ( have_rows('site_fields') ) : the_row(); ?>

						<?php while ( have_rows('site_section_fields') ) : the_row();

				        	if (get_sub_field('summary') == 'land'): ?>

				        		<span class="half"><?php if (get_sub_field('text')): ?><?php the_sub_field('text'); ?><?php elseif (get_sub_field('value')): ?><?php the_sub_field('value'); ?><?php endif; ?></span>

				        	<?php endif;

						endwhile;

				    endwhile;

				endif; ?>

			</div>

			<div class="field">

				<h6 class="half">Power available</h6>

				<?php if( have_rows('site_fields') ):

				    while ( have_rows('site_fields') ) : the_row(); ?>

						<?php while ( have_rows('site_section_fields') ) : the_row();

				        	if (get_sub_field('summary') == 'power-available'): ?>

				        		<span class="half"><?php if (get_sub_field('text')): ?><?php the_sub_field('text'); ?><?php elseif (get_sub_field('value')): ?><?php the_sub_field('value'); ?><?php endif; ?></span>

				        	<?php endif;

						endwhile;

				    endwhile;

				endif; ?>

			</div>

			<div class="field">

				<h6 class="half">...in 12 months</h6>

				<?php if( have_rows('site_fields') ):

				    while ( have_rows('site_fields') ) : the_row(); ?>

						<?php while ( have_rows('site_section_fields') ) : the_row();

				        	if (get_sub_field('summary') == 'power-available-in-12-months'): ?>

				        		<span class="half"><?php if (get_sub_field('text')): ?><?php the_sub_field('text'); ?><?php elseif (get_sub_field('value')): ?><?php the_sub_field('value'); ?><?php endif; ?></span>

				        	<?php endif;

						endwhile;

				    endwhile;

				endif; ?>

			</div>

			<div class="field">

				<h6 class="half">...in 18 months</h6>

				<?php if( have_rows('site_fields') ):

				    while ( have_rows('site_fields') ) : the_row(); ?>

						<?php while ( have_rows('site_section_fields') ) : the_row();

				        	if (get_sub_field('summary') == 'power-available-in-18-months'): ?>

				        		<span class="half"><?php if (get_sub_field('text')): ?><?php the_sub_field('text'); ?><?php elseif (get_sub_field('value')): ?><?php the_sub_field('value'); ?><?php endif; ?></span>

				        	<?php endif;

						endwhile;

				    endwhile;

				endif; ?>

			</div>

			<div class="field">

				<h6 class="half">...in 24 months</h6>

				<?php if( have_rows('site_fields') ):

				    while ( have_rows('site_fields') ) : the_row(); ?>

						<?php while ( have_rows('site_section_fields') ) : the_row();

				        	if (get_sub_field('summary') == 'power-available-in-24-months'): ?>

				        		<span class="half"><?php if (get_sub_field('text')): ?><?php the_sub_field('text'); ?><?php elseif (get_sub_field('value')): ?><?php the_sub_field('value'); ?><?php endif; ?></span>

				        	<?php endif;

						endwhile;

				    endwhile;

				endif; ?>				

			</div>			

			<div class="field">

				<h6 class="half">...in 36 months</h6>

				<?php if( have_rows('site_fields') ):

				    while ( have_rows('site_fields') ) : the_row(); ?>

						<?php while ( have_rows('site_section_fields') ) : the_row();

				        	if (get_sub_field('summary') == 'power-available-in-36-months'): ?>

				        		<span class="half"><?php if (get_sub_field('text')): ?><?php the_sub_field('text'); ?><?php elseif (get_sub_field('value')): ?><?php the_sub_field('value'); ?><?php endif; ?></span>

				        	<?php endif;

						endwhile;

				    endwhile;

				endif; ?>

			</div>	

			<div class="field">

				<h6 class="half">Water available</h6>

				<?php if( have_rows('site_fields') ):

				    while ( have_rows('site_fields') ) : the_row(); ?>

						<?php while ( have_rows('site_section_fields') ) : the_row();

				        	if (get_sub_field('summary') == 'water-available'): ?>

				        		<span class="half"><?php if (get_sub_field('text')): ?><?php the_sub_field('text'); ?><?php elseif (get_sub_field('value')): ?><?php the_sub_field('value'); ?><?php endif; ?></span>

				        	<?php endif;

						endwhile;

				    endwhile;

				endif; ?>

			</div>	

			<div class="field">

				<h6 class="half">Sewer available</h6>

				<?php if( have_rows('site_fields') ):

				    while ( have_rows('site_fields') ) : the_row(); ?>

						<?php while ( have_rows('site_section_fields') ) : the_row();

				        	if (get_sub_field('summary') == 'sewer-available'): ?>

				        		<span class="half"><?php if (get_sub_field('text')): ?><?php the_sub_field('text'); ?><?php elseif (get_sub_field('value')): ?><?php the_sub_field('value'); ?><?php endif; ?></span>

				        	<?php endif;

						endwhile;

				    endwhile;

				endif; ?>

			</div>

			<div class="field">

				<h6 class="half">Fiber</h6>
				<span class="half">Text</span>

			</div>

		</div>

		<div class="half">

			<div class="field">

				<h1><?php the_title(); ?></h1>

			</div>

			<div class="site-status">

				<div class="field">

					<h6>Site status</h6>

					<?php


					if( have_rows('site_fields') ):

					    while ( have_rows('site_fields') ) : the_row(); ?>

							<?php while ( have_rows('site_section_fields') ) : the_row();

								$field_name = get_sub_field('field_name');
								$letters = array("?", ".", "!", "(", ")", " ");
								$status = get_sub_field('status');

								if(get_sub_field('leveltoview') == 'level3') {
									if ($user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
										echo siteStatus($field_name, $letters, $status);
									}
								}

								if(get_sub_field('leveltoview') == 'level2') {
									if ($user_role == 'level2' || $user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
										echo siteStatus($field_name, $letters, $status);
									}
								}

								if(get_sub_field('leveltoview') == 'level1') {
									if ($user_role == 'level1' || $user_role == 'level2' || $user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
										echo siteStatus($field_name, $letters, $status);
									}
								}

							endwhile;

					    endwhile;

					?>
					<div class="clear"></div>
					<?php

					else :

					    // no layouts found

					endif;

					?>					
				</div>

				<div class="status-explanation">

					<div class="field ready-to-build-explanation">
						<h6 class="half">Ready to build</h6>
					</div>

					<div class="field in-development-explanation">
						<h6 class="half">In development</h6>
					</div>

					<div class="field not-ready-explanation">
						<h6 class="half">Not ready</h6>
					</div>

				</div>

			</div>

		</div>

	</section>

	<section class="site-field">

	<?php

	if( have_rows('site_fields') ):

	    while ( have_rows('site_fields') ) : the_row(); ?>

			<h1><?php the_sub_field('site_section_name'); ?></h1>

			<?php while ( have_rows('site_section_fields') ) : the_row();

	        if( get_row_layout() == 'text' ) {
				
				$leveltoview = get_sub_field('leveltoview');
				$field_name = get_sub_field('field_name');
				$text = get_sub_field('text');
				$deadline = get_sub_field('deadline');
				$status = get_sub_field('status');
				$person_responsible = get_sub_field('person_responsible');
				$source = get_sub_field('source');
				$comment = get_sub_field('comment');

				if($leveltoview == 'level3') {
					if ($user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo textLayout($field_name, $text, $deadline, $status, $person_responsible, $source, $comment);
					}
				}

				if($leveltoview == 'level2') {
					if ($user_role == 'level2' || $user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo textLayout($field_name, $text, $deadline, $status, $person_responsible, $source, $comment);
					}
				}

				if($leveltoview == 'level1') {
					if ($user_role == 'level1' || $user_role == 'level2' || $user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo textLayout($field_name, $text, $deadline, $status, $person_responsible, $source, $comment);
					}
				}

			} elseif( get_row_layout() == 'value' ) {

				$field = get_sub_field_object('unit');
				$value = get_sub_field('value');
				$label = get_sub_field('unit');
				$label = $field['choices'][ $label ];
				$field_name = get_sub_field('field_name');
				$text = get_sub_field('text');
				$deadline = get_sub_field('deadline');
				$status = get_sub_field('status');
				$person_responsible = get_sub_field('person_responsible');
				$source = get_sub_field('source');
				$comment = get_sub_field('comment');

				if(get_sub_field('leveltoview') == 'level3') {
					if ($user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo valueLayout($field_name, $value, $deadline, $status, $person_responsible, $source, $comment, $label);
					}
				}

				if(get_sub_field('leveltoview') == 'level2') {
					if ($user_role == 'level2' || $user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo valueLayout($field_name, $value, $deadline, $status, $person_responsible, $source, $comment, $label);
					}
				}

				if(get_sub_field('leveltoview') == 'level1') {
					if ($user_role == 'level1' || $user_role == 'level2' || $user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo valueLayout($field_name, $value, $deadline, $status, $person_responsible, $source, $comment, $label);
					}
				}

			} elseif( get_row_layout() == 'yes_or_no' ) {

				$leveltoview = get_sub_field('leveltoview');
				$field_name = get_sub_field('field_name');
				$yesno = get_sub_field('text');
				$deadline = get_sub_field('deadline');
				$status = get_sub_field('status');
				$person_responsible = get_sub_field('person_responsible');
				$source = get_sub_field('source');
				$comment = get_sub_field('comment');

				if (get_sub_field('yes')) {
					$yesno = 'Yes';
				} else {
					$yesno = 'No';
				}

				if($leveltoview == 'level3') {
					if ($user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo textLayout($field_name, $yesno, $deadline, $status, $person_responsible, $source, $comment);
					}
				}

				if($leveltoview == 'level2') {
					if ($user_role == 'level2' || $user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo textLayout($field_name, $yesno, $deadline, $status, $person_responsible, $source, $comment);
					}
				}

				if($leveltoview == 'level1') {
					if ($user_role == 'level1' || $user_role == 'level2' || $user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo textLayout($field_name, $yesno, $deadline, $status, $person_responsible, $source, $comment);
					}
				}

			} elseif( get_row_layout() == 'image' ) {

				$leveltoview = get_sub_field('leveltoview');
				$field_name = get_sub_field('field_name');
				$image = get_sub_field('image');
				$deadline = get_sub_field('deadline');
				$status = get_sub_field('status');
				$person_responsible = get_sub_field('person_responsible');
				$source = get_sub_field('source');
				$comment = get_sub_field('comment');

				if($leveltoview == 'level3') {
					if ($user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo imageLayout($field_name, $image, $deadline, $status, $person_responsible, $source, $comment);
					}
				}

				if($leveltoview == 'level2') {
					if ($user_role == 'level2' || $user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo imageLayout($field_name, $image, $deadline, $status, $person_responsible, $source, $comment);
					}
				}

				if($leveltoview == 'level1') {
					if ($user_role == 'level1' || $user_role == 'level2' || $user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo imageLayout($field_name, $image, $deadline, $status, $person_responsible, $source, $comment);
					}
				}

			} elseif( get_row_layout() == 'gallery' ) {

				$leveltoview = get_sub_field('leveltoview');
				$field_name = get_sub_field('field_name');
				$images = get_sub_field('images');
				$deadline = get_sub_field('deadline');
				$status = get_sub_field('status');
				$person_responsible = get_sub_field('person_responsible');
				$source = get_sub_field('source');
				$comment = get_sub_field('comment');

				if($leveltoview == 'level3') {
					if ($user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo galleryLayout($field_name, $images, $deadline, $status, $person_responsible, $source, $comment);
					}
				}

				if($leveltoview == 'level2') {
					if ($user_role == 'level2' || $user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo galleryLayout($field_name, $images, $deadline, $status, $person_responsible, $source, $comment);
					}
				}

				if($leveltoview == 'level1') {
					if ($user_role == 'level1' || $user_role == 'level2' || $user_role == 'level3' || $user_role == 'administrator' || $user_role == 'admin') {
						echo galleryLayout($field_name, $images, $deadline, $status, $person_responsible, $source, $comment);
					}
				}

			}

			endwhile;

	    endwhile;

	else :

	    // no layouts found

	endif;

	?>

	</section>

</div>

<?php } } ?>
<?php get_footer(); ?>