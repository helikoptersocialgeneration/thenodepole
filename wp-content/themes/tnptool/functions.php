<?php

//Hide the controls for content providers
$user = wp_get_current_user();
if ( in_array( 'content_provider', (array) $user->roles ) ) {
	function hide_controls() {
	  echo '<style>

		.acf-hl,
		[data-name="add_to_site_summary"],
		[data-name="status"],
		[data-name="user_data_access_level"] {
			display: none!important;
		}

	  </style>
	  ';
	}
	add_action('admin_head', 'hide_controls');
}

function remove_menu_items(){
    remove_menu_page( 'edit.php?post_type=acf-field-group' );
    remove_menu_page( 'wpseo_dashboard' );
    remove_menu_page( 'edit.php?post_type=page' ); 
    remove_menu_page( 'edit.php' );
    remove_menu_page( 'edit-comments.php' );
    remove_menu_page( 'themes.php' ); 
    remove_menu_page( 'plugins.php' );
    remove_menu_page( 'tools.php' );
    remove_menu_page( 'options-general.php' );
    remove_menu_page( 'index.php' );
    // remove_menu_page( 'acf-options' );
}
add_action( 'admin_menu', 'remove_menu_items', 999 );

function cptui_register_my_cpts() {
    $labels = array(
        "name" => "Sites",
        "singular_name" => "Site",
        "menu_name" => "Sites",
        "all_items" => "All Sites",
        "add_new" => "Add New",
        "add_new_item" => "Add New Site",
        "edit" => "Edit",
        "edit_item" => "Edit Site",
        "new_item" => "New Site",
        "view" => "View",
        "view_item" => "View Site",
        "search_items" => "Search Site",
        "not_found" => "No Sites found",
        "not_found_in_trash" => "No Sites found in Trash",
        "parent" => "Parent Site",
        );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "sites", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "revisions" ),
    );
    register_post_type( "sites", $args );

    $labels = array(
        "name" => "Sections",
        "singular_name" => "Section",
        "menu_name" => "Sections",
        "all_items" => "All Sections",
        "add_new" => "Add New",
        "add_new_item" => "Add New Section",
        "edit" => "Edit",
        "edit_item" => "Edit Section",
        "new_item" => "New Section",
        "view" => "View",
        "view_item" => "View Section",
        "search_items" => "Search Section",
        "not_found" => "No Sections found",
        "not_found_in_trash" => "No Sections found in Trash",
        "parent" => "Parent Section",
        );

    $args = array(
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "show_ui" => true,
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "sections", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "revisions" ),
    );
    register_post_type( "sections", $args );
}
add_action( 'init', 'cptui_register_my_cpts' );

// Registrering a connection type
function my_connection_types() {
	p2p_register_connection_type( array(
	  'name' => 'sites_to_sections',
	  'from' => 'sites',
	  'to' => 'sections',
	  'sortable' => 'any',
	  'title' => array(
	    'from' => __( 'Sections', 'my-textdomain' ),
	  ),
	  'admin_column' => 'to'
	) );
}
add_action( 'p2p_init', 'my_connection_types' );

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Settings',
		'menu_title'	=> 'Settings',
		'menu_slug' 	=> 'settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}
?>