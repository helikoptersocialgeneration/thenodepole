jQuery(document).ready(function($){
	//update these values if you change these breakpoints in the style.css file (or _layout.scss if you use SASS)
	var MqM= 768,
		MqL = 1024;

	var sitesSections = $('.tnptool-sites-group'),
		sitesTrigger = $('.tnptool-sites-trigger'),
		sitesContainer = $('.tnptool-sites-items'),
		sitesCategoriesContainer = $('.tnptool-sites-sections'),
		sitesCategories = sitesCategoriesContainer.find('a'),
		closesitesContainer = $('.tnptool-close-panel');
	
	//select a sites section 
	sitesCategories.on('click', function(event){
		event.preventDefault();
		var selectedHref = $(this).attr('href'),
			target= $(selectedHref);
		if( $(window).width() < MqM) {
			sitesContainer.scrollTop(0).addClass('slide-in').children('ul').removeClass('selected').end().children(selectedHref).addClass('selected');
			closesitesContainer.addClass('move-left');
			$('body').addClass('tnptool-overlay');
		} else {
	        $('body,html').animate({ 'scrollTop': target.offset().top - 19}, 200); 
		}
	});

	//close sites lateral panel - mobile only
	$('body').bind('click touchstart', function(event){
		if( $(event.target).is('body.tnptool-overlay') || $(event.target).is('.tnptool-close-panel')) { 
			closePanel(event);
		}
	});
	sitesContainer.on('swiperight', function(event){
		closePanel(event);
	});

	//show sites content clicking on sitesTrigger
	sitesTrigger.on('click', function(event){
		event.preventDefault();
		$(this).next('.tnptool-sites-content').slideToggle(0);
		$(this).next().next('.tnptool-sites-content').slideToggle(0).end().parent('li').toggleClass('content-visible');
		$(this).next('.tnptool-sites-previewtext').fadeToggle(0);
	});

	$('.tnptool-sites-previewtext').on('click', function(event){
		event.preventDefault();
		$(this).next('.tnptool-sites-content').slideToggle(0).end().parent('li').toggleClass('content-visible');
		$(this).fadeToggle(0);
	});

	//update category sidebar while scrolling
	$(window).on('scroll', function(){
		if ( $(window).width() > MqL ) {
			(!window.requestAnimationFrame) ? updateCategory() : window.requestAnimationFrame(updateCategory); 
		}
	});

	$(window).on('resize', function(){
		if($(window).width() <= MqL) {
			sitesCategoriesContainer.removeClass('is-fixed').css({
				'-moz-transform': 'translateY(0)',
			    '-webkit-transform': 'translateY(0)',
				'-ms-transform': 'translateY(0)',
				'-o-transform': 'translateY(0)',
				'transform': 'translateY(0)',
			});
		}	
		if( sitesCategoriesContainer.hasClass('is-fixed') ) {
			sitesCategoriesContainer.css({
				'left': sitesContainer.offset().left,
			});
		}
	});

	function closePanel(e) {
		e.preventDefault();
		sitesContainer.removeClass('slide-in').find('li').show();
		closesitesContainer.removeClass('move-left');
		$('body').removeClass('tnptool-overlay');
	}

	function updateCategory(){
		updateCategoryPosition();
		updateSelectedCategory();
	}

	function updateCategoryPosition() {
		var top = $('.tnptool-sites').offset().top,
			height = jQuery('.tnptool-sites').height() - jQuery('.tnptool-sites-sections').height(),
			margin = 20;
		if( top - margin <= $(window).scrollTop() && top - margin + height > $(window).scrollTop() ) {
			var leftValue = sitesCategoriesContainer.offset().left,
				widthValue = sitesCategoriesContainer.width();
			sitesCategoriesContainer.addClass('is-fixed').css({
				'left': leftValue,
				'top': margin,
				'-moz-transform': 'translateZ(0)',
			    '-webkit-transform': 'translateZ(0)',
				'-ms-transform': 'translateZ(0)',
				'-o-transform': 'translateZ(0)',
				'transform': 'translateZ(0)',
			});
		} else if( top - margin + height <= $(window).scrollTop()) {
			var delta = top - margin + height - $(window).scrollTop();
			sitesCategoriesContainer.css({
				'-moz-transform': 'translateZ(0) translateY('+delta+'px)',
			    '-webkit-transform': 'translateZ(0) translateY('+delta+'px)',
				'-ms-transform': 'translateZ(0) translateY('+delta+'px)',
				'-o-transform': 'translateZ(0) translateY('+delta+'px)',
				'transform': 'translateZ(0) translateY('+delta+'px)',
			});
		} else { 
			sitesCategoriesContainer.removeClass('is-fixed').css({
				'left': 0,
				'top': 94,
			});
		}
	}

	function updateSelectedCategory() {
		sitesSections.each(function(){
			var actual = $(this),
				margin = parseInt($('.tnptool-sites-title').eq(1).css('marginTop').replace('px', '')),
				activeCategory = $('.tnptool-sites-sections a[href="#'+actual.attr('id')+'"]'),
				topSection = (activeCategory.parent('li').is(':first-child')) ? 0 : Math.round(actual.offset().top);
			
			if ( ( topSection - 20 <= $(window).scrollTop() ) && ( Math.round(actual.offset().top) + actual.height() + margin - 20 > $(window).scrollTop() ) ) {
				activeCategory.addClass('selected');
			}else {
				activeCategory.removeClass('selected');
			}
		});
	}

	$('#view-all').click(function(){
		$('#view-summary').removeClass('current');
		$(this).addClass('current');

		$('.summary-headline').hide();
		$('.not-site-summary, .tnptool-sites-title').show();
	});

	$('#view-summary').click(function(){
		$('#view-all').removeClass('current');
		$(this).addClass('current');

		$('.not-site-summary, .tnptool-sites-title').hide();
		$('.summary-headline, .summary-headline .tnptool-sites-title').show();
	});
});