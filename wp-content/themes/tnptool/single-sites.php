<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo bloginfo('template_url'); ?>/css/app.css"> <!-- CSS reset -->
	<script src="<?php echo bloginfo('template_url'); ?>/js/modernizr.js"></script> <!-- Modernizr -->
	<title>Data Center Site - <?php the_title(); ?></title>
</head>
<body>

<header>
	<h1><?php the_title(); ?></h1>
</header>
<section class="tnptool-sites">

	<?php if(get_field('info')) { ?>
	<h2>Information</h2>
	<div class="tnptool-intro">
		<?php the_field('info'); ?>
	</div>
	<?php } ?>
	
	<?php
		$related = p2p_type('sites_to_sections')->get_related(get_queried_object()); 
		$user = wp_get_current_user();
	?>

	<ul class="tnptool-sites-sections">
		<p><?php if(is_user_logged_in()) { ?><b>User Information:</b><br>
		<b>Username:</b> <?php echo $user->data->display_name; ?><br>
		<b>Access:</b> <?php echo $user->roles[0]; ?><br><br>
		<?php } ?><b>Based on currencies:</b><br>
		<b>Dollar:</b> <?php echo get_field('dollar', 'option'); ?><br>
		<b>Last Update:</b> <?php echo get_field('dollar_last_update', 'option'); ?><br><br>
		<b>Euro:</b> <?php echo get_field('euro', 'option'); ?><br>
		<b>Last Update:</b> <?php echo get_field('euro_last_update', 'option'); ?><br><br>
		<b>Pound:</b> <?php echo get_field('pound', 'option'); ?><br>
		<b>Last Update:</b> <?php echo get_field('pound_last_update', 'option'); ?>
		</p>
		<?php foreach ($related->query['connected_items'] as $key => $post_id) { ?>

			<?php if( have_rows('site_section_fields', $post_id) ) {
				$status = array();
			    while ( have_rows('site_section_fields', $post_id) ) { the_row();
			        if( get_row_layout() == 'text' || get_row_layout() == 'value' || get_row_layout() == 'image' || get_row_layout() == 'gallery' || get_row_layout() == 'yes_or_no' ) {
			       		array_push($status, get_sub_field('status'));
			        }
			    }

				if (in_array("not_ready", $status)) {
					$set_status = "not_ready_group";
				} elseif (in_array("in_development", $status)) {
					$set_status = "in_development_group";
				} else {
					$set_status = "ready_to_build_group";
				}


			} ?>

			<li><a class="selected <?php echo $set_status; ?>" href="#<?php echo str_replace(' ', '', get_the_title($post_id)); ?>"><?php echo get_the_title($post_id); ?></a></li>
		<?php } ?>
	</ul>
	<div class="tnptool-sites-items">
		<div class="toggle-summary">
			<div id="view-all" class="current">Show All</div>
			<div id="view-summary">Show Summary</div>
		</div>
		<ul class="tnptool-sites-group summary-headline">
			<li class="tnptool-sites-title"><h2>Site Summary</h2></li>
		</ul>
		<?php

		if(!is_user_logged_in()) {
    		$user->roles[0] = 'data_user_access_level_1';
    	}

		foreach ($related->query['connected_items'] as $key => $post_id) { ?>
			<ul id="<?php echo str_replace(' ', '', get_the_title($post_id)); ?>" class="tnptool-sites-group">
				 <li class="tnptool-sites-title"><h2><?php echo get_the_title($post_id); ?></h2></li>
			<?php if( have_rows('site_section_fields', $post_id) ) {
			    while ( have_rows('site_section_fields', $post_id) ) { the_row();
			        if( get_row_layout() == 'text' ) { ?>

			        	<?php
			        	$access_level_field = get_sub_field('user_data_access_level');
						$access_level = array($access_level_field, 'administrator', 'content_provider', 'cheif_of_fields');

						if(count(array_intersect($access_level, (array) $user->roles)) > 0){
						$status = get_sub_field('status');
				        ?>

						<li class="<?php if(!get_sub_field('add_to_site_summary')) { echo "not-site-summary "; } echo $status; ?>">
							<a class="tnptool-sites-trigger  <?php echo $status; ?>_text" href="#0"><?php the_sub_field('field_name'); ?></a>
							<div class="tnptool-sites-previewtext"><?php if(get_sub_field('text')) { ?><?php the_sub_field('text', false, false); ?><?php } ?></div>
							<div class="tnptool-sites-content">
								<?php if(get_sub_field('text')) { ?><p><b>Text</b><?php the_sub_field('text', false, false); ?></p><?php } ?>
								<?php if(get_sub_field('deadline')) { ?><p><b>Deadline</b><?php the_sub_field('deadline'); ?></p><?php } ?>
								<?php if(get_sub_field('person_responsible')) { ?><p><b>Person Responsible</b><?php the_sub_field('person_responsible'); ?></p><?php } ?>
								<?php if(get_sub_field('source_name')) { ?><p><b>Source</b><?php if(get_sub_field('source_link')) { ?><a target="_blank" href="<?php the_sub_field('source_link'); ?>"><?php } ?><?php the_sub_field('source_name'); ?><?php if(get_sub_field('source_link')) { ?></a><?php } ?></p><?php } ?>
								<?php if(get_sub_field('comment')) { ?><p><b>Comment</b><?php the_sub_field('comment', false, false); ?></p><?php } ?>
							</div> <!-- tnptool-sites-content -->
						</li>

						<?php } ?>

			        <?php } elseif ( get_row_layout() == 'value' ) { ?>

						<?php
			        	$access_level_field = get_sub_field('user_data_access_level');
						$access_level = array($access_level_field, 'administrator', 'content_provider', 'cheif_of_fields');

						if(count(array_intersect($access_level, (array) $user->roles)) > 0){
						$status = get_sub_field('status');

						$unit = get_sub_field('unit');

						if($unit == 'm2') {
							$value = 'm2 - ' . get_sub_field('value') . '<br>';
							$value .= 'Acres - ' . 0.000247105381 * get_sub_field('value') . '<br>';
							$value .= 'Hectares - ' . 0.0001 * get_sub_field('value') . '<br>';
						} elseif($unit == 'kr') {
							$value = 'Krona - ' . get_sub_field('value') . '<br>';
							$value .= 'Dollar - ' . get_field('dollar', 'option') * get_sub_field('value') . '<br>';
							$value .= 'Euro - ' . get_field('euro', 'option') * get_sub_field('value') . '<br>';
							$value .= 'Pound - ' . get_field('pound', 'option') * get_sub_field('value');
						} elseif($unit == 'meter') {
							$value = 'Meter - ' . get_sub_field('value') . '<br>';
							$value .= 'Feet - ' . 3.2808399 * get_sub_field('value') . '<br>';
						}

				        ?>

						<li class="<?php if(!get_sub_field('add_to_site_summary')) { echo "not-site-summary "; } echo $status; ?>">
							<a class="tnptool-sites-trigger <?php echo $status; ?>_text" href="#0"><?php the_sub_field('field_name'); ?></a>
							<div class="tnptool-sites-previewtext"><?php if(get_sub_field('text')) { ?><?php the_sub_field('text', false, false); ?><?php } ?></div>
							<div class="tnptool-sites-content">
								<?php if(get_sub_field('text')) { ?><p><b>Text</b><?php the_sub_field('text', false, false); ?></p><?php } ?>
								<?php if(get_sub_field('value')) { ?><p><b>Value</b><?php echo $value; ?></p><?php } ?>
								<?php if(get_sub_field('deadline')) { ?><p><b>Deadline</b><?php the_sub_field('deadline'); ?></p><?php } ?>
								<?php if(get_sub_field('person_responsible')) { ?><p><b>Person Responsible</b><?php the_sub_field('person_responsible'); ?></p><?php } ?>
								<?php if(get_sub_field('source_name')) { ?><p><b>Source</b><?php if(get_sub_field('source_link')) { ?><a target="_blank" href="<?php the_sub_field('source_link'); ?>"><?php } ?><?php the_sub_field('source_name'); ?><?php if(get_sub_field('source_link')) { ?></a><?php } ?></p><?php } ?>
								<?php if(get_sub_field('comment')) { ?><p><b>Comment</b><?php the_sub_field('comment', false, false); ?></p><?php } ?>
							</div> <!-- tnptool-sites-content -->
						</li>

						<?php } ?>

			        <?php } elseif ( get_row_layout() == 'image' ) { ?>

						<?php
			        	$access_level_field = get_sub_field('user_data_access_level');
						$access_level = array($access_level_field, 'administrator', 'content_provider', 'cheif_of_fields');

						if(count(array_intersect($access_level, (array) $user->roles)) > 0){
						$status = get_sub_field('status');
				        ?>

						<li class="<?php if(!get_sub_field('add_to_site_summary')) { echo "not-site-summary "; } echo $status; ?>">
							<a class="tnptool-sites-trigger <?php echo $status; ?>_text" href="#0"><?php the_sub_field('field_name'); ?> - Image</a>
							<div class="tnptool-sites-content">
								<?php if(get_sub_field('image')) { ?><p><b>Image</b><img data-featherlight="<?php echo get_sub_field('image')['url']; ?>" width="25%" src="<?php echo get_sub_field('image')['url']; ?>"></p><?php } ?>
								<?php if(get_sub_field('deadline')) { ?><p><b>Deadline</b><?php the_sub_field('deadline'); ?></p><?php } ?>
								<?php if(get_sub_field('person_responsible')) { ?><p><b>Person Responsible</b><?php the_sub_field('person_responsible'); ?></p><?php } ?>
								<?php if(get_sub_field('source_name')) { ?><p><b>Source</b><?php if(get_sub_field('source_link')) { ?><a target="_blank" href="<?php the_sub_field('source_link'); ?>"><?php } ?><?php the_sub_field('source_name'); ?><?php if(get_sub_field('source_link')) { ?></a><?php } ?></p><?php } ?>
								<?php if(get_sub_field('comment')) { ?><p><b>Comment</b><?php the_sub_field('comment', false, false); ?></p><?php } ?>
							</div> <!-- tnptool-sites-content -->
						</li>

						<?php } ?>

			        <?php } elseif ( get_row_layout() == 'gallery' ) { ?>

						<?php
			        	$access_level_field = get_sub_field('user_data_access_level');
						$access_level = array($access_level_field, 'administrator', 'content_provider', 'cheif_of_fields');

						if(count(array_intersect($access_level, (array) $user->roles)) > 0){
						$status = get_sub_field('status');
				        ?>

						<li class="<?php if(!get_sub_field('add_to_site_summary')) { echo "not-site-summary "; } echo $status; ?>">
							<a class="tnptool-sites-trigger <?php echo $status; ?>_text" href="#0"><?php the_sub_field('field_name'); ?> - Gallery</a>
							<div class="tnptool-sites-content">
								<?php if ( have_rows('images') ) { ?>
								<div><b>Gallery</b>
								<?php while ( have_rows('images') ) { the_row(); ?>
									<div class="gallery-image">
										<img class="lightbox" data-comment="<?php echo get_sub_field('comment'); ?>" data-title="<?php echo get_sub_field('title'); ?>" data-featherlight="<?php echo get_sub_field('image')['url']; ?>" src="<?php echo get_sub_field('image')['sizes']['thumbnail']; ?>">
									</div>
								<?php } ?>
								</div>
								<?php } ?>
								<?php if(get_sub_field('deadline')) { ?><p><b>Deadline</b><?php the_sub_field('deadline'); ?></p><?php } ?>
								<?php if(get_sub_field('person_responsible')) { ?><p><b>Person Responsible</b><?php the_sub_field('person_responsible'); ?></p><?php } ?>
								<?php if(get_sub_field('source_name')) { ?><p><b>Source</b><?php if(get_sub_field('source_link')) { ?><a target="_blank" href="<?php the_sub_field('source_link'); ?>"><?php } ?><?php the_sub_field('source_name'); ?><?php if(get_sub_field('source_link')) { ?></a><?php } ?></p><?php } ?>
								<?php if(get_sub_field('comment')) { ?><p><b>Comment</b><?php the_sub_field('comment', false, false); ?></p><?php } ?>
							</div> <!-- tnptool-sites-content -->
						</li>

						<?php } ?>

			        <?php } elseif ( get_row_layout() == 'yes_or_no' ) { ?>

						<?php
			        	$access_level_field = get_sub_field('user_data_access_level');
						$access_level = array($access_level_field, 'administrator', 'content_provider', 'cheif_of_fields');

						if(count(array_intersect($access_level, (array) $user->roles)) > 0){
						$status = get_sub_field('status');
				        ?>

						<li class="<?php if(!get_sub_field('add_to_site_summary')) { echo "not-site-summary "; } echo $status; ?>">
							<a class="tnptool-sites-trigger <?php echo $status; ?>_text" href="#0"><?php the_sub_field('field_name'); ?> - <span class="yesorno"><?php if(get_sub_field('yes')) { echo "Yes"; } else { echo "No"; } ?></span></a>
							<div class="tnptool-sites-previewtext"><?php if(get_sub_field('text')) { ?><?php the_sub_field('text', false, false); ?><?php } ?></div>
							<div class="tnptool-sites-content">
								<?php if(get_sub_field('text')) { ?><p><b>Text</b><?php the_sub_field('text', false, false); ?></p><?php } ?>
								<p><b>Yes or No</b><?php if(get_sub_field('yes')) { echo "Yes"; } else { echo "No"; } ?></p>
								<?php if(get_sub_field('deadline')) { ?><p><b>Deadline</b><?php the_sub_field('deadline'); ?></p><?php } ?>
								<?php if(get_sub_field('person_responsible')) { ?><p><b>Person Responsible</b><?php the_sub_field('person_responsible'); ?></p><?php } ?>
								<?php if(get_sub_field('source_name')) { ?><p><b>Source</b><?php if(get_sub_field('source_link')) { ?><a target="_blank" href="<?php the_sub_field('source_link'); ?>"><?php } ?><?php the_sub_field('source_name'); ?><?php if(get_sub_field('source_link')) { ?></a><?php } ?></p><?php } ?>
								<?php if(get_sub_field('comment')) { ?><p><b>Comment</b><?php the_sub_field('comment', false, false); ?></p><?php } ?>
							</div> <!-- tnptool-sites-content -->
						</li>

						<?php } ?>

			        <?php }
				}
			} ?>
			</ul>
		<?php } ?>
	</div>
	<a href="#0" class="tnptool-close-panel">Close</a>
</section> <!-- tnptool-sites -->
<script src="<?php echo bloginfo('template_url'); ?>/js/jquery-2.1.1.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/js/jquery.mobile.custom.min.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/js/main.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/js/featherlight.js"></script>

</body>
</html>